import { Component, OnInit } from '@angular/core';
import { TitleShareSerivce } from 'src/app/ui/top-panel/title-share.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less']
})
export class MainComponent implements OnInit {
  constructor(private titleShareService: TitleShareSerivce) {}

  ngOnInit() {
    this.titleShareService.emitChange('Предложения сезона');
  }
}

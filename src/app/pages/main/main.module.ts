import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main-routing.module';
import { InvestModalModule } from 'src/app/ui/invest-modal/invest-modal.module';
import { ProductsBlockModule } from 'src/app/ui/products-block/products-block.module';
import { ProductsBlock2Module } from 'src/app/ui/products-block2/products-block2.module';
import { AnalyticsModule } from 'src/app/ui/analytics/analytics.module';

@NgModule({
  declarations: [MainComponent],
  imports: [
    MainRoutingModule,
    InvestModalModule,
    ProductsBlockModule,
    ProductsBlock2Module,
    AnalyticsModule
  ]
})
export class MainModule {}

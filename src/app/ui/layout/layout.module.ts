import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { HeaderModule } from '../header/header.module';
import { FooterModule } from '../footer/footer.module';
import { HeaderMobileModule } from '../header-mobile/header-mobile.module';
import { SidebarModule } from '../sidebar/sidebar.module';
import { TopPanelModule } from '../top-panel/top-panel.module';

@NgModule({
  declarations: [LayoutComponent],
  imports: [
    RouterModule,
    CommonModule,
    HeaderModule,
    FooterModule,
    HeaderMobileModule,
    SidebarModule,
    TopPanelModule
  ],
  exports: [LayoutComponent]
})
export class LayoutModule {}

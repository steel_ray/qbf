import { NgModule } from '@angular/core';
import { SidebarComponent } from './sidebar.component';
import { MenuModule } from '../menu/menu.module';

@NgModule({
  declarations: [SidebarComponent],
  imports: [MenuModule],
  exports: [SidebarComponent, MenuModule]
})
export class SidebarModule {}

import { NgModule } from '@angular/core';
import { HeaderMobileComponent } from './header-mobile.component';

@NgModule({
  declarations: [HeaderMobileComponent],
  exports: [HeaderMobileComponent]
})
export class HeaderMobileModule {}

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable()
export class TitleShareSerivce {
  private emitChangeSource = new Subject<string>();
  changeEmitted$ = this.emitChangeSource.asObservable();
  emitChange(change: string) {
    this.emitChangeSource.next(change);
  }
}

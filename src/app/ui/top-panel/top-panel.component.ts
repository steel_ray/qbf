import { Component, OnInit } from '@angular/core';
import { TitleShareSerivce } from './title-share.service';

@Component({
  selector: 'app-top-panel',
  templateUrl: './top-panel.component.html',
  styleUrls: ['./top-panel.component.less']
})
export class TopPanelComponent implements OnInit {
  title: string;
  constructor(private titleShareSerivce: TitleShareSerivce) {}

  ngOnInit() {
    this.titleShareSerivce.changeEmitted$.subscribe(text => {
      this.title = text;
    });
  }
}

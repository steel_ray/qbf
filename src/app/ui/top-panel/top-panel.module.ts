import { NgModule } from '@angular/core';
import { TopPanelComponent } from './top-panel.component';
import { TitleShareSerivce } from './title-share.service';

@NgModule({
  declarations: [TopPanelComponent],
  exports: [TopPanelComponent],
  providers: [TitleShareSerivce]
})
export class TopPanelModule {}

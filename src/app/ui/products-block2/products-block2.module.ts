import { NgModule } from '@angular/core';
import { ProductsBlock2Component } from './products-block2.component';

@NgModule({
  declarations: [ProductsBlock2Component],
  exports: [ProductsBlock2Component]
})
export class ProductsBlock2Module {}

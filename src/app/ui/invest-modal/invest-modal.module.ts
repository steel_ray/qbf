import { NgModule } from '@angular/core';
import { InvestModalComponent } from './invest-modal.component';

@NgModule({
  declarations: [InvestModalComponent],
  exports: [InvestModalComponent]
})
export class InvestModalModule {}

import { NgModule } from '@angular/core';
import { ProductsBlockComponent } from './products-block.component';

@NgModule({
  declarations: [ProductsBlockComponent],
  exports: [ProductsBlockComponent]
})
export class ProductsBlockModule {}
